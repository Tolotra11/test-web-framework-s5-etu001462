<%-- 
    Document   : liste
    Created on : 18 nov. 2022, 16:20:08
    Author     : Tolotra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<% ArrayList<String> list = (ArrayList<String>)request.getAttribute("myListe"); %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Voici une liste</h1>
        <% for(int i = 0;i<list.size();i++) {%>
            <p><%= list.get(i) %></p>
        <% } %>
    </body>
</html>

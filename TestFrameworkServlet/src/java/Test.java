
import annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import modelview.ModelView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Tolotra
 */
public class Test {

    private double d;

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    @Annotation(url = "testFormulaire")
    public ModelView myChiffre() {
        ModelView mv = new ModelView();
        mv.setUrl("test.jsp");
        HashMap<String, Object> data = new HashMap<>();
        data.put("chiffre", d);
        mv.setData(data);
        return mv;
    }

    @Annotation(url = "listeTest")
    public ModelView liste() {
        ArrayList<String> liste = new ArrayList<>();
        liste.add("Bonjour");
        liste.add("Bonsoir");
        liste.add("Bon apres midi");
        liste.add("Au revoir");
         ModelView mv = new ModelView();
        mv.setUrl("liste.jsp");
        HashMap<String, Object> data = new HashMap<>();
        data.put("myListe", liste);
        mv.setData(data);
        return mv;
    }
}
